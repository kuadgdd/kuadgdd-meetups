# KNIME User+Developer Group Dresden (KUADGDD)

The [KNIME User+Developer Group Dresden](https://www.meetup.com/de-DE/Dresden-KNIME-Users/) meetup group has the purpose of promoting the awareness, use and adoption of [KNIME](https://www.knime.com), the open source predictive analytics platform. 
Membership and meetup attendance is open to all KNIME users, those interested in KNIME, as well as those interested in extending their analytics capability. 
The aim is to hold meetups where real-life experiences of using KNIME are aired, new developments in KNIME are explained with some deep dives into KNIME capabilities. 
As an open analytics platform, KNIME can work with R, WEKA and many other tools - the interoperability of KNIME will also be covered. 
The objective is to build a connected community of KNIME users and all those interested in analytics. 
Join up, and find out who else is interested in KNIME. If you have a story around KNIME or analytics - please contact us on [Meetup](https://www.meetup.com/de-DE/Dresden-KNIME-Users).

Organizer: Daniel Esser, Philipp Katz, Klemens Muthmann, Jacobo Miranda

## 2018

### Dresden KNIME Meetup #1 (01/09/18 7:00 PM)

* [Meetup.com](https://www.meetup.com/de-DE/Dresden-KNIME-Users/events/245527031/)
* [Agenda](https://lineupr.com/kuadgdd/meetup-18-01)
* [Presentation: Kilian Thiel - 1. KNIME Meetup](https://bitbucket.org/kuadgdd/kuadgdd-meetups-public/downloads/2018_01_09_Dresden-KNIME-Meetup_Kilian.pdf)
* [Presentation: Jacobo Miranda - The Way to KNIME](https://bitbucket.org/kuadgdd/kuadgdd-meetups-public/downloads/2018_01_09_Dresden-KNIME-Meetup_Miranda.pdf)

### Dresden KNIME Meetup #2: Deep Learning (05/17/18 6:30 PM)

* [Meetup.com](https://www.meetup.com/de-DE/Dresden-KNIME-Users/events/250416123/)
* [Agenda](https://lineupr.com/kuadgdd/meetup)
* [Presentation: Christian Dietz - KNIME Deep Learning](https://bitbucket.org/kuadgdd/kuadgdd-meetups-public/downloads/2018_05_17_Dresden-KNIME-Meetup-Christian.pdf)
* [Presentation: Jacobo Miranda - NodePit By The Numbers](https://bitbucket.org/kuadgdd/kuadgdd-meetups-public/downloads/2018_05_17_Dresden-KNIME-Meetup-Miranda.pdf)